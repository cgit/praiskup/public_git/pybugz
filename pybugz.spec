%global gitrev 89df2
%global posttag git%{gitrev}
%global snapshot %{version}-%{posttag}

Name:       pybugz
Summary:    Command line interface for Bugzilla written in Python
Version:    0.10
Release:    1.%{posttag}%{?dist}
Group:      Applications/Communications
License:    GPLv2
URL:        https://github.com/williamh/pybugz
BuildArch:  noarch

Requires:       python2
BuildRequires:  python2-devel

%if ! 0%{?rhel}
# no bash-completion for RHEL
%global bash_completion 1
%endif

%if %{?bash_completion}
BuildRequires: bash-completion pkgconfig
%endif

# There is possible to download upstream tarball generated by github, but it is
# quite old now.  For HOWTO obtain correct tarball see the "prepare-tarball.sh"
# script.
Source0: %{name}-%{snapshot}.tar.gz

# follow https://github.com/praiskup/pybugz changes (until accepted by upstream)
Patch0: %{name}-%{snapshot}-downstream.patch
# make the installation better satisfy RHEL / Fedora purposes
Patch1: %{name}-%{snapshot}-rhel-fedora-cust.patch

%description
Pybugz was conceived as a tool to speed up the work-flow for Gentoo Linux
contributors when dealing with bugs using Bugzilla.  By avoiding the clunky web
interface, the user can search, isolate and contribute to the project very
quickly.  Developers alike can easily extract attachments and close bugs
comfortably from the command line.

%prep
%setup -q -n %{name}-%{snapshot}
%patch0 -p1 -b .downstream
%patch1 -p1 -b .rhel-fedora-cust

%build
%{__python} setup.py build

%install
# default install process
%{__python} setup.py install --root=%{buildroot}

%global bash_cmpl_dir %(pkg-config --variable=completionsdir bash-completion)
%if %{?bash_completion}
  # find the proper directory to install bash-completion script
  mkdir -p %{buildroot}%{bash_cmpl_dir}
  cp %{_builddir}/%{name}-%{snapshot}/contrib/bash-completion \
     %{buildroot}%{bash_cmpl_dir}/bugz
%endif

mkdir -p %{buildroot}%{_mandir}/man1
mv man/bugz.1 %{buildroot}%{_mandir}/man1/bugz.1
mkdir -p %{buildroot}%{_docdir}

%clean

%files
%{_bindir}/bugz
%{python_sitelib}/bugz
%if %{?bash_completion}
  %{bash_cmpl_dir}/bugz
%endif
%{python_sitelib}/%{name}-*.egg-info
%{_mandir}/man1/bugz.1.gz
%config(noreplace) %{_sysconfdir}/pybugz
%doc README LICENSE

%changelog
* Sun Jan 20 2013 Pavel Raiskup <praiskup@redhat.com> - 0.10-1.git89df2
- changes for problems spotted/fixed by Scott Tsai in merge-review bug:
- important change - move git revision behind the release number
- reflect that ^^^ change in changelog
- remove statement disabling debuginfo (it is not needed)

* Sun Jan 20 2013 Pavel Raiskup <praiskup@redhat.com> - 0.10git69cd7-1
- apply downstream patches to reflect https://github.com/praiskup/pybugz
  it allows hierarchy of configuration files and a bit better error handling
- update URL as upstream is now on github
- make the RedHat bugzilla default, s/bugz/pybugz/ in manpage
- fedora-review fixes: s/define/global/, BR python2-devel, noreplace
- mention in documentation the ~/.bugzrc file
- switch the binary name to 'bugz' again, it would mislead users (see merge
  review bug comment)

* Mon Oct 01 2012 Pavel Raiskup <praiskup@redhat.com> - 0.10-1
- rebase to 0.10
- use the 'pybugz' rather then bugz which collides a little with 'bugzilla'

* Tue Nov 30 2010 Pierre Carrier <prc@redhat.com> - 0.8.0-1
- Initial packaging
