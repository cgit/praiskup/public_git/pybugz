#!/bin/bash

# grep the spec file for version number
VERSION=$( cat pybugz.spec | grep ^Version: | cut -d' ' -f 2- | tr -d ' ')
REV=$( cat pybugz.spec | grep '%global gitrev' | cut -d' ' -f 3- | tr -d ' ')

BASE="pybugz-${VERSION}-git${REV}"
TARBALL=$BASE.tar.gz
DIR=$( mktemp -d )
GIT=https://github.com/williamh/pybugz.git

echo == preparing tarball for pybugz-$VERSION ==

pushd $DIR > /dev/null                                                  && \
git clone $GIT pybugz                                                   && \
cd pybugz                                                               && \
git archive --prefix $BASE/ $REV | gzip > $TARBALL                      && \
popd > /dev/null                                                        && \
cp $DIR/pybugz/$TARBALL .                                               && \
echo == DONE ==                                                         && \
rm -rf $DIR
